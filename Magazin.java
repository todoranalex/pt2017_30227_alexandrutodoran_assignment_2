import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Magazin implements ActionListener {

    private final int nrOfQueues = 3;
    private static int minEntranceTime = 0;
    private static int maxEtranceTime = 0;
    private static float averageWaitingTime = 0;
    private int currentSecond = 0;
    private static int interval = 0;
    private static int nrOfClients = 0;
    private static int minClientServiceTime = 0;
    private static int maxClientServiceTime = 0;
    private int timeBeforeApp = 15;
    private static int applicationRuntime = 0;

    private static boolean checkTimer = false;


    private ArrayList<Coada> cozi = new ArrayList<>(nrOfQueues);
    public ArrayList<Thread> threads = new ArrayList<>(nrOfQueues);
    private TimerClass timer;
    private static final Logger LOGGER = Logger.getLogger(LogEvent.class.getName());
    private Random random = new Random();


    public Magazin(int nothing){
    }


    public void createDelay() throws InterruptedException{
        Timer t = new Timer(1000, this);
        t.start();
        TimeUnit.SECONDS.sleep(timeBeforeApp);
        t.stop();

    }

    public void getDataFromGUI() throws InterruptedException{
        timer = new TimerClass(this.cozi);

        createDelay();

        interval = timer.getInterval();
        minEntranceTime = timer.getMinArrivalTime();
        maxEtranceTime = timer.getMaxArrivalTime();
        minClientServiceTime = timer.getMinClientServiceTime();
        maxClientServiceTime = timer.getMaxClientServiceTime();
    }

    public void createThreads(){

        for (int i = 0; i < nrOfQueues; i++) {

            cozi.add(i, new Coada("Queue_" + i));
            threads.add(i, new Thread(cozi.get(i)));
            threads.get(i).setName("Thread_" + i);

        }
    }

    public void startThreads(){
        for (Thread thread: threads) {
            thread.start();
        }
    }

    public void generateClients() throws InterruptedException{
        while (currentSecond <= interval) {

            currentSecond++;
            applicationRuntime = timer.getApplicationRunTime();

            if(currentSecond == applicationRuntime){
                for (Thread thread : threads) {
                    System.out.println("--STOPPING-- " + thread.getName());
                    LOGGER.log(Level.INFO,"--STOPPING-- " + thread.getName());
                    thread.interrupt();
                }
                break;
            }
            int randomServiceTime = random.nextInt(maxClientServiceTime - minClientServiceTime + 1) + minClientServiceTime; // random intre 1 si 10
            Client client = new Client(randomServiceTime, "Client_" + nrOfClients);


            int getRightQueue = getMinQueueServiceTime();

            averageWaitingTime += cozi.get(getRightQueue).getServiceTimeOfQueue();// cat trebuie sa astepte un client , fara timpul lui;


            cozi.get(getRightQueue).addClient(client);

            int randomTime = random.nextInt(maxEtranceTime - minEntranceTime + 1) + minEntranceTime;
            nrOfClients++;
            TimeUnit.SECONDS.sleep(randomTime);
        }
    }

    public void checkTimer(){
        while (checkTimer == false) {
            int ok = 0;
            for (int nr = 0; nr < nrOfQueues; nr++) {
                if (cozi.get(nr).getSize() == 0) {
                    ok++;
                }
            }
            if (ok == 3) {
                checkTimer = true;
            }
        }

    }

    public void stopThreads(){
        for (Thread thread : threads) {

            LOGGER.log(Level.INFO,"--STOPPING-- " + thread.getName());
            thread.interrupt();
            LOGGER.log(Level.INFO,"--END-- ");

        }
    }

    public Magazin () throws InterruptedException {

        getDataFromGUI();
        createThreads();
        startThreads();
        generateClients();
        checkTimer();
        stopThreads();
    }

    public int getMinQueueServiceTime() {
        int minServiceTimeofQueues = Integer.MAX_VALUE;
        int minQueue = 0;

        for (int i = 0; i < nrOfQueues; i++) {
            if (cozi.get(i).getServiceTimeOfQueue() < minServiceTimeofQueues) {
                minServiceTimeofQueues = cozi.get(i).getServiceTimeOfQueue();
                minQueue = i;
            }
        }
        return minQueue;
    }

    public void actionPerformed(ActionEvent e){
        System.out.println((timeBeforeApp--));

    }

    public int getNrOfClients(){
        return nrOfClients;
    }

    public float getAverageWaitTime(){
        return averageWaitingTime;
    }

    public boolean getcheckTime(){
        return checkTimer;
    }
}



