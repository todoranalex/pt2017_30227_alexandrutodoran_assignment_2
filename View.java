import java.awt.*;
import java.util.Observable;
import java.util.Observer;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JLabel;

public class View extends JFrame {

    private JPanel mainPanel = new JPanel();
    private JLabel Coada0 = new JLabel("Queue0: ");
    private JLabel Coada1 = new JLabel("Queue1: ");
    private JLabel Coada2 = new JLabel("Queue2: ");
    private JLabel Timer = new JLabel("Current second: ");
    private JLabel TimeShow = new JLabel();
    private JLabel Show0 = new JLabel();
    private JLabel Show1 = new JLabel();
    private JLabel Show2 = new JLabel();
    private JLabel ShowInfo0 = new JLabel();
    private JLabel ShowInfo1 = new JLabel();
    private JLabel ShowInfo2 = new JLabel();
    private JLabel ShowAvg0 = new JLabel("Average Coada0 TBD");
    private JLabel ShowAvg1 = new JLabel("Average Coada1 TBD");
    private JLabel ShowAvg2 = new JLabel("Average Coada2 TBD");
    private JLabel ShowWait = new JLabel("Average WaitTime TBD");
    private JLabel ShowPeak = new JLabel("PEAK TIME TBD");
    private JLabel ShowEmpty = new JLabel("Average empty time TBD");


    private JButton button = new JButton("Start");
    private JButton button1 = new JButton("Get Data");

    private JTextField getSimulationInterval = new JTextField();
    private JTextField getClientTime = new JTextField();
    private JTextField getMinArrivalTime = new JTextField();
    private JTextField getMaxArrivalTime = new JTextField();
    private JTextField getMinServiceTime = new JTextField();
    private JTextField getMaxServiceTime = new JTextField();

    private JLabel getSimInterval = new JLabel("Simulation Interval: ");
    private JLabel getClTime = new JLabel("Client generator Interval: ");
    private JLabel getMinArrTime = new JLabel("Min Arrive Time: ");
    private JLabel getMaxArrTime = new JLabel("Max Arrive Time: ");
    private JLabel getMinServTime = new JLabel("Min Service Time: ");
    private JLabel getMaxServTime = new JLabel("Max Service Time: ");


    private void addComponents(){

        Timer.setBounds(50,0,100,70);
        TimeShow.setBounds(150,0,30,70);

        button.setBounds(220,0,70,30);
        button1.setBounds(900,410,100,30);

        Coada0.setBounds(10,50,60,70);
        Coada1.setBounds(10,150,60,70);
        Coada2.setBounds(10,250,60,70);

        Show0.setBounds(70,50,800,70);
        Show1.setBounds(70,150,800,70);
        Show2.setBounds(70,250,800,70);

        ShowInfo0.setBounds(300,90,500,70);
        ShowInfo1.setBounds(300,190,500,70);
        ShowInfo2.setBounds(300,290,500,70);

        ShowAvg0.setBounds(10,330,500,70);
        ShowAvg1.setBounds(10,350,500,70);
        ShowAvg2.setBounds(10,370,500,70);
        ShowWait.setBounds(10,390,500,70);
        ShowPeak.setBounds(10,410,500,70);
        ShowEmpty.setBounds(10,430,500,70);

        getSimulationInterval.setBounds(800,310,100,30);
        getClientTime.setBounds(800,350,100,30);
        getMinArrivalTime.setBounds(800,390,100,30);
        getMaxArrivalTime.setBounds(800,430,100,30);
        getMinServiceTime.setBounds(800,470,100,30);
        getMaxServiceTime.setBounds(800,510,100,30);

        getSimInterval.setBounds(680,310,130,30);
        getClTime.setBounds(650,350,150,30);
        getMinArrTime.setBounds(680,390,130,30);
        getMaxArrTime.setBounds(680,430,130,30);
        getMinServTime.setBounds(680,470,130,30);
        getMaxServTime.setBounds(680,510,130,30);


        mainPanel.add(Coada0);
        mainPanel.add(Coada1);
        mainPanel.add(Coada2);

        mainPanel.add(button);
        mainPanel.add(button1);

        mainPanel.add(Show0);
        mainPanel.add(Show1);
        mainPanel.add(Show2);
        mainPanel.add(ShowInfo0);
        mainPanel.add(ShowInfo1);
        mainPanel.add(ShowInfo2);
        mainPanel.add( ShowPeak);
        mainPanel.add( ShowAvg0);
        mainPanel.add( ShowAvg1);
        mainPanel.add( ShowAvg2);
        mainPanel.add( ShowWait);
        mainPanel.add(ShowEmpty);

        mainPanel.add(Timer);
        mainPanel.add(TimeShow);

        mainPanel.add(getSimulationInterval);
        mainPanel.add(getClientTime);
        mainPanel.add(getMinArrivalTime);
        mainPanel.add(getMaxArrivalTime);
        mainPanel.add(getMinServiceTime);
        mainPanel.add(getMaxServiceTime);

        mainPanel.add(getSimInterval);
        mainPanel.add(getClTime);
        mainPanel.add(getMinArrTime);
        mainPanel.add(getMaxArrTime);
        mainPanel.add(getMinServTime);
        mainPanel.add(getMaxServTime);

    }

    private void jFrameSetup(){
        setSize(1000,600);
        setResizable(false);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
       // setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    }


    public void setResult0(String res){
        Show0.setText(res);
    }
    public void setResult1(String res){
        Show1.setText(res);
    }
    public void setResult2(String res){
        Show2.setText(res);
    }
    public void setResultTimer(String res){
        TimeShow.setText(res);
    }

    public void attachButton(ActionListener a) {
        button.addActionListener(a);
    }
    public void attachButton1(ActionListener a){ button1.addActionListener(a);}



    public void setInfo0(String res){
        ShowInfo0.setText(res);
    }
    public void setInfo1(String res){
        ShowInfo1.setText(res);
    }
    public void setInfo2(String res){
        ShowInfo2.setText(res);
    }



    public void setShowAvg0(String res) { ShowAvg0.setText(res);}
    public void setShowAvg1(String res){
        ShowAvg1.setText(res);
    }
    public void setShowAvg2(String res){
        ShowAvg2.setText(res);
    }
    public void setShowWait(String res){
        ShowWait.setText(res);
    }
    public void setShowPeak(String res){
        ShowPeak.setText(res);
    }
    public void setShowEmpty(String res) {ShowEmpty.setText(res);}


    public int getAppRuntime() {
        return Integer.parseInt(getSimulationInterval.getText());}
    public int getClientTime(){
        return Integer.parseInt(getClientTime.getText());
    }

    public int getMinArrivalTime(){
        return Integer.parseInt(getMinArrivalTime.getText());
    }
    public int getMaxArrivalTime(){
        return Integer.parseInt(getMaxArrivalTime.getText());
    }
    public int getMinServiceTime(){
        return Integer.parseInt(getMinServiceTime.getText());
    }
    public int getMaxServiceTime(){
        return Integer.parseInt(getMaxServiceTime.getText());
    }



    public View(){
        add(mainPanel);
        mainPanel.setLayout(null);
        addComponents();
        jFrameSetup();
    }

}
