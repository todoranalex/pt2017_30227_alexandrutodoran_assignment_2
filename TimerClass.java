import java.awt.event.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

public class TimerClass  implements ActionListener  {

    private View view;
    private Timer timer;
    private static ArrayList<Coada> cozi;
    private static final Logger LOGGER = Logger.getLogger(LogEvent.class.getName());

    private  int currentSecond ;
    private int maxSecond = 0;
    private int maxClients = 0;
    private static int applicationRunTime = 0;
    private final int nrCozi = 3;
    private int averageEmptyTime = 0;
    private static  int minArrivalTime = 0;
    private static int maxArrivalTime = 0;
    private static int minClientServiceTime = 0;
    private static int maxClientServiceTime = 0;
    private static int interval = 0;

    public TimerClass(ArrayList<Coada> cozi) throws InterruptedException{

        this.cozi = cozi;
        view = new View();
        view.attachButton(new AddListenerStartButton());
        view.attachButton1(new AddListenerGetDataButton());
        timer = new Timer(1000,this);

    }

    class AddListenerStartButton implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            timer.start();
        }
    }

    class AddListenerGetDataButton implements ActionListener{
        public void actionPerformed(ActionEvent e){
            applicationRunTime = view.getAppRuntime();
            interval = view.getClientTime();
            minArrivalTime = view.getMinArrivalTime();
            maxArrivalTime = view.getMaxArrivalTime();
            minClientServiceTime = view.getMinServiceTime();
            maxClientServiceTime = view.getMaxServiceTime();
        }
    }

    public void actionPerformed(ActionEvent e) {
       // System.out.println("Timer was at second " + currentSecond);
        LOGGER.log(Level.INFO,"--TIMER--Timer was at second " + currentSecond);


        currentSecond++;
        int currentClientsinAllQueues = 0;

        for (Coada coada : cozi) {
            currentClientsinAllQueues += coada.getSize();
            averageEmptyTime += coada.checkEmptyQueue();
        }
        if (currentClientsinAllQueues > maxClients) {
            maxClients = currentClientsinAllQueues;
            maxSecond = currentSecond;
        }

        view.setResultTimer("" + currentSecond);

        view.setResult0(cozi.get(0).showQueue());
        view.setResult1(cozi.get(1).showQueue());
        view.setResult2(cozi.get(2).showQueue());

        view.setInfo0("Currently in "+ cozi.get(0).getName()+" : TOTAL clients: " + cozi.get(0).getSize() + " | TOTAL ST : " + cozi.get(0).getServiceTimeOfQueue());
        view.setInfo1("Currently in "+ cozi.get(1).getName()+" : TOTAL clients: " + cozi.get(1).getSize() + " | TOTAL ST : " + cozi.get(1).getServiceTimeOfQueue());
        view.setInfo2("Currently in "+ cozi.get(2).getName()+" : TOTAL clients: " + cozi.get(2).getSize() + " | TOTAL ST : " + cozi.get(2).getServiceTimeOfQueue());


        //System.out.println("At second " + currentSecond + " there are in Magazin " + currentClientsinAllQueues);
        LOGGER.log(Level.INFO,"--TIMER--At second " + currentSecond + " there are in Magazin " + currentClientsinAllQueues);

        Magazin magazin1 = new Magazin(1);

        if ((currentSecond == applicationRunTime) || (magazin1.getcheckTime()==true)) {

           // System.out.println("The timer stopped at second " + currentSecond);
            LOGGER.log(Level.INFO,"--TIMER-- The timer stopped at second " + currentSecond);
           // System.out.println("At second " + maxSecond + " there were a maximum total of " + maxClients + " clients in Magazin");
            view.setShowPeak("PEAK TIME : At second  "+ maxSecond + " there were a maximum total of " + maxClients + " clients in Magazin" );
            //System.out.println("Average empty time/ 3 queues: " + averageEmptyTime / nrCozi);


            view.setShowAvg0("Average service time in " + cozi.get(0).getName() + " is " + cozi.get(0).getAverageServiceTime() / cozi.get(0).getTotalClients() + " seconds");
            view.setShowAvg1("Average service time in " + cozi.get(1).getName() + " is " + cozi.get(1).getAverageServiceTime() / cozi.get(1).getTotalClients() + " seconds");
            view.setShowAvg2("Average service time in " + cozi.get(2).getName() + " is " + cozi.get(2).getAverageServiceTime() / cozi.get(2).getTotalClients() + " seconds");

            view.setShowWait("Average waiting time for a new client: "+ magazin1.getAverageWaitTime()/magazin1.getNrOfClients());
            view.setShowEmpty("Average empty queue time/3 queues : " + averageEmptyTime/nrCozi);

            LOGGER.log(Level.INFO,"--TIMER--Application Terminated " );
            timer.stop();

        }

    }

    public int getApplicationRunTime(){
        return applicationRunTime;
    }
    public int getInterval(){
        return interval;
    }
    public int getMinArrivalTime(){
        return minArrivalTime;
    }
    public int getMaxArrivalTime(){
        return maxArrivalTime;
    }
    public int getMinClientServiceTime(){
        return minClientServiceTime;
    }
    public int getMaxClientServiceTime(){
        return maxClientServiceTime;
    }


    public static void main(String[] args) throws InterruptedException{
        LogEvent.init();
        Magazin magazin = new Magazin();
    }
}
