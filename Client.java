/**
 * Created by Alex on 3/28/2017.
 */
public class Client {

    private int serviceTime;
    private String name;


    public Client(int serviceTime,String name){
        this.serviceTime = serviceTime;
        this.name = name;
    }

    public int getServiceTime(){
        return serviceTime;
    }
    public String getName(){
        return name;
    }

    public String toString(){
        return  name ;
    }


}
