import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Coada implements Runnable {

    private LinkedBlockingQueue<Client> clienti = new LinkedBlockingQueue<>();
    private String name;
    private float averageServiceTime = 0;
    private int totalClients = 0;

    private static final Logger LOGGER = Logger.getLogger(LogEvent.class.getName());

    public Coada(String name){
        this.name = name;
    }


    public void addClient(Client client) {
       // System.out.println("--ARRIVAL--" + client.getName() + " enters "+ name);
        LOGGER.log(Level.INFO,"--ARRIVAL--" + client.getName() + " enters "+ name);
        clienti.add(client);
    }

    public int getSize() {
        return clienti.size();
    }

    public int getServiceTimeOfQueue() {

        int serviceTime = 0;
        for (Client cl : clienti)
            serviceTime += cl.getServiceTime();

        return serviceTime;
    }

    public String showQueue() {
        String s = new String("");
        for (Client cl : clienti)
            s += " " + cl.toString() + " | ST: "+cl.getServiceTime()+" ";
        return s;
    }

    public void run()  {
        //System.out.println("--STARTING-- " + Thread.currentThread().getName() +" representing " +name);
        LOGGER.log(Level.INFO,"--STARTING-- " + Thread.currentThread().getName() +" representing " +name);

        while (true) {
            if (!clienti.isEmpty()) {
              //  System.out.println("--INFO-- The number of clients in  " + name + " is " + getSize());
                LOGGER.log(Level.INFO,"--INFO-- The number of clients in  " + name + " is " + getSize());
                processClient();
            }

        }
    }

    public void processClient ()  {

        Client client;
        client = clienti.peek();
        averageServiceTime += client.getServiceTime();
        totalClients ++;

        //System.out.println("--PROCESSING-- " + client.getName() + " having Service Time " + client.getServiceTime() + " seconds");
        LOGGER.log(Level.INFO,"--PROCESSING-- " + client.getName() + " having Service Time " + client.getServiceTime() + " seconds");

        try {
            TimeUnit.SECONDS.sleep(client.getServiceTime());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        clienti.remove();
        //System.out.println("--EXIT-- " + client.getName() + " has finished and quits " + name);
        LOGGER.log(Level.INFO,"--EXIT-- " + client.getName() + " has finished and quits " + name);

    }

    public String getName(){
        return name;
    }

    public int getTotalClients(){
        return totalClients;
    }
    public float getAverageServiceTime(){
        return averageServiceTime;
    }


    public int checkEmptyQueue(){
        if (clienti.isEmpty())
            return 1;
        else
            return 0;
    }

}